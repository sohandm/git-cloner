# git-cloner

Docker image to clone requested github or bitbucket repository.

This is a fork of [git-cloner by stakater](https://github.com/stakater-docker/git-cloner) with a few fixes and added features including sub-directory cloning.

## Use case

Kubernetes pods where an init container usually clones a git repository into an emptyDir volume which will be mounted to other containers in the pod to share between them.

## Environment variables

### required
`REPO_LINK` - github or bitbucket repository SSH clone link

It is also required to mount a volume where the repository will be cloned from local file system into container `/repository` directory.

In case of private repositories you also have to mount deployment SSH key authorized to clone code repository into `/key` directory.

### optional
`REPO_TAG` - checkout specified tag

`REPO_REVISION` - checkout specified revision

`REPO_BRANCH` - clone specified branch (defaults to master)

`REPO_SUBDIR` - clone only a sub-directory (set this in situations where you need to clone only a single sub-directory)

`REPO_KEY` - RSA key filename (defaults to id_rsa)

if cloning using repository username/password instead SSH deployment key, please provide `REPO_LINK` without leading `https://`

`REPO_USER` - authorized user to clone requested repository

`REPO_PASS` - authorized user password to clone requested repository


## Example run

```
docker run --rm -ti \
    -v /path/to/clone/repository:/repository \
    -v /path/to/authorized/id_rsa:/key:ro \
    -e REPO_LINK=git@gitlab.com:sohandm/git-cloner.git
    -e REPO_BRANCH=master
    -e REPO_TAG=1.0.0
    registry.gitlab.com/sohandm/git-cloner
```

