FROM alpine:3.9
MAINTAINER  Sohan Muthukumara "m.sohan@directfn.com"

RUN apk add --update git openssh

RUN addgroup -g 1000 -S git && \
    adduser -u 1000 -S git -G git

COPY [--chown=git] init.sh /

RUN mkdir /home/git/.ssh/  && \
    touch /home/git/.ssh/known_hosts && \
    chown -R git /home/git/.ssh && \
	  mkdir /temp-clone /repository && \
    chown git /temp-clone /repository && \
	  echo "" >> /etc/ssh/ssh_config && \
    echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config &&\
    chown git /etc/ssh/ssh_config

USER git

CMD ["/init.sh"]
